package com.sg.serviceimplTest;

import com.sg.bean.Employee;
import com.sg.bean.EmployeeTO;
import com.sg.repositoryimpl.EmployeeRegistrationRepoImpl;
import com.sg.requestbean.EmployeeRequest;
import com.sg.serviceimpl.EmployeeRegisterServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceImplTest {
    @InjectMocks
    private EmployeeRegisterServiceImpl employeeRegisterService;

    @Mock
    private EmployeeRegistrationRepoImpl employeeRegistrationRepo;

    EmployeeRequest employeeRequest;

    Employee employee;

    @Before
    public void setUp() throws Exception {
        employeeRequest = new EmployeeRequest();
        employeeRequest.setFirstname("hemant");
        employeeRequest.setLastName("kumar");
        employeeRequest.setDepartment("Development");
        employeeRequest.setDob("01Feb1992");
        employeeRequest.setGender("male");

        employee = new Employee();
        employee.setDepartment("DEV");
        employee.setDob("01Feb1992");
        employee.setLastName("Kumar");
        employee.setGender("male");
        employee.setFirstname("Hemant");
        employee.setEmpid(8);
        employee.setLastName("kumar");
    }

    @Test
    public void testregisterEmployee() throws Exception {
        try {
            employeeRegisterService.registerEmployee(employeeRequest);
        } catch (Exception e) {

        }
    }

    @Test
    public void testgetAllEmployeeList() throws Exception {
        List<Employee> employees = new ArrayList<>();
        employees.add(employee);
        Mockito.when(employeeRegistrationRepo.getAllEmployee()).thenReturn(employees);
        List<EmployeeTO> employeeTOS = employeeRegisterService.getAllEmployeeList();
        Assert.assertNotNull(employeeTOS);
    }
}
