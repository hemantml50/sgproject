package com.sg.repository;

import com.sg.bean.Employee;

import java.util.List;

public interface EmployeeRegistrationRepo {
    public void registerEmployee(Employee employeedetails)throws Exception;

    public List<Employee> getAllEmployee()throws Exception;
}
