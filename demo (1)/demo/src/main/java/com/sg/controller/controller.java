package com.sg.controller;

import com.sg.bean.EmployeeTO;
import com.sg.requestbean.EmployeeRequest;
import com.sg.serviceimpl.EmployeeRegisterServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.management.openmbean.OpenDataException;
import java.util.List;

@RestController
public class controller {

    public static final Logger logger = LoggerFactory.getLogger(controller.class);
    public static final String BASE_URL = "api/v1/employee";

    @Autowired
    private EmployeeRegisterServiceImpl employeeRegisterService;

    @RequestMapping(value = BASE_URL + "/register", method = RequestMethod.POST)
    public ResponseEntity<Object> registerEmployee(@RequestBody  EmployeeRequest employeeRequest)throws Exception {

        try {
            employeeRegisterService.registerEmployee(employeeRequest);
        } catch (Exception e) {
            return new ResponseEntity<Object>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<Object>("employee registered successfully", HttpStatus.CREATED);

    }

    @RequestMapping(value = BASE_URL + "/getall", method = RequestMethod.GET)
    public ResponseEntity<Object> getAllEmployee()throws Exception {
        List<EmployeeTO> allEmployeeList = null;
        try {
            allEmployeeList = employeeRegisterService.getAllEmployeeList();
        } catch (Exception e) {
            return new ResponseEntity<Object>("Something went Wrong", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Object>(allEmployeeList, HttpStatus.CREATED);

    }

}
