package com.sg.serviceimpl;

import com.sg.bean.Employee;
import com.sg.bean.EmployeeTO;
import com.sg.repositoryimpl.EmployeeRegistrationRepoImpl;
import com.sg.requestbean.EmployeeRequest;
import com.sg.service.EmployeeRegisterService;
import org.springframework.beans.InvalidPropertyException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.management.openmbean.OpenDataException;
import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeRegisterServiceImpl implements EmployeeRegisterService {

    @Autowired
    private EmployeeRegistrationRepoImpl employeeRegistrationRepo;


    @Override
    public void registerEmployee(EmployeeRequest employeeRequest)throws Exception {
        validateRequest(employeeRequest);
        try {
            if (null != employeeRequest) {
                Employee employee = new Employee();
                employee.setFirstname(employeeRequest.getFirstname());
                employee.setLastName(employeeRequest.getLastName());
                employee.setGender(employeeRequest.getGender());
                employee.setDob(employeeRequest.getDob());
                employee.setDepartment(employeeRequest.getDepartment());

                employeeRegistrationRepo.registerEmployee(employee);

            }
        } catch (Exception e) {
            throw new OpenDataException();
        }

    }

    private void validateRequest(EmployeeRequest employeeRequest)throws Exception {
        if(null==employeeRequest||null==employeeRequest.getFirstname()||null==employeeRequest.getLastName()||
                null==employeeRequest.getDepartment()||null==employeeRequest.getDob()||
                null==employeeRequest.getGender()){
            throw new InvalidKeyException("Invalid Input");
        }
    }

    @Override
    public List<EmployeeTO> getAllEmployeeList() throws Exception {
        List<EmployeeTO> employeeTOS = new ArrayList<>();
        try {
            List<Employee> employees = employeeRegistrationRepo.getAllEmployee();

            if (!CollectionUtils.isEmpty(employees)) {
                for (Employee employee : employees) {
                    EmployeeTO employeeTO = new EmployeeTO();
                    employeeTO.setEmpid(employee.getEmpid());
                    employeeTO.setFirstname(employee.getFirstname());
                    employeeTO.setLastName(employee.getFirstname());
                    employeeTO.setDob(employee.getDob());
                    employeeTO.setGender(employee.getGender());
                    employeeTO.setDepartment(employee.getDepartment());
                    employeeTOS.add(employeeTO);
                }
            }

        } catch (Exception e) {
            throw new OpenDataException();
        }
        return employeeTOS;
    }
}
