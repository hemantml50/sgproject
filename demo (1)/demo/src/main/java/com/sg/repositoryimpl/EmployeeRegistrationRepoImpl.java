package com.sg.repositoryimpl;

import com.sg.bean.Employee;
import com.sg.repository.EmployeeRegistrationRepo;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.management.openmbean.OpenDataException;
import java.util.List;

@Repository
public class EmployeeRegistrationRepoImpl implements EmployeeRegistrationRepo {

    public static final Logger logger = LoggerFactory.getLogger(EmployeeRegistrationRepoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;


    @Override
    public void registerEmployee(Employee employeedetails) throws Exception {
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Transaction tx = session.beginTransaction();
            session.save(employeedetails);
            tx.commit();

        } catch (Exception e) {
            logger.info("register Employee Details : {}", employeedetails);
            throw new OpenDataException();
        } finally {
            session.close();
        }
    }

    @Override
    public List<Employee> getAllEmployee() throws Exception {
        List<Employee> employee = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Criteria criteria = session.createCriteria(Employee.class);
            employee = criteria.list();
        } catch (Exception e) {
            logger.info("get all employee list : {}");
            throw new OpenDataException();
        } finally {
            session.close();
        }
        return employee;
    }
}
