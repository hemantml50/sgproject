package com.sg.service;

import com.sg.bean.EmployeeTO;
import com.sg.requestbean.EmployeeRequest;

import java.util.List;

public interface EmployeeRegisterService {

    public void registerEmployee(EmployeeRequest employeeRequest)throws Exception ;

    public List<EmployeeTO> getAllEmployeeList()throws Exception ;
}
